import sqlite3
import uuid
from random import random
import networkx as nx

# allow to store uuids in sqlite 
sqlite3.register_converter('UUID', lambda b: uuid.UUID(bytes=b))
sqlite3.register_adapter(uuid.UUID, lambda u: u.bytes)

if __name__ == "__main__":
	# use in memory sqlite db
	with sqlite3.connect(':memory:', detect_types=sqlite3.PARSE_DECLTYPES) as conn:

		print("Opened database successfully");

		# settings for better performance
		conn.execute('PRAGMA temp_store=MEMORY')
		conn.execute('PRAGMA journal_mode=MEMORY')

		# check foreign keys for debugging
		conn.execute('PRAGMA foreign_keys')

		# set up database schema
		conn.executescript('''
			SELECT 1;
			BEGIN TRANSACTION;

			DROP TABLE IF EXISTS "timestep";
			--- the timestep table contains on row per simulation step
			--- counting from 0
			CREATE TABLE IF NOT EXISTS "timestep" (
				"time"	INTEGER NOT NULL UNIQUE,
				PRIMARY KEY("time" AUTOINCREMENT)
			);

			--- a node is an agent in a network
			--- the capacity is the number of signals
			--- it can process per timestep
			DROP TABLE IF EXISTS "node";
			CREATE TABLE IF NOT EXISTS "node" (
				"uuid"	UUID NOT NULL UNIQUE,
				"capacity"	INTEGER NOT NULL DEFAULT 1,
				PRIMARY KEY("uuid")
			);

			--- an edge connects to agents
			--- an edge goes only in one direction
			--- but it is possible to create a pair of symmetric edges
			--- the feedback signal of a message is send backwards through the edge
			--- so the feedback reaches the sender even if the edges are not symmetric
			DROP TABLE IF EXISTS "edge";
			CREATE TABLE IF NOT EXISTS "edge" (
				"uuid"	UUID NOT NULL UNIQUE,
				"source"	UUID NOT NULL,
				"target"	UUID NOT NULL,
				"delay"	INTEGER NOT NULL DEFAULT 1,
				UNIQUE("source","target"),
				CHECK("source" <> "target"),
				FOREIGN KEY("source") REFERENCES "node"("uuid") ON DELETE CASCADE ON UPDATE CASCADE,
				FOREIGN KEY("target") REFERENCES "node"("uuid") ON DELETE CASCADE ON UPDATE CASCADE,
				PRIMARY KEY("uuid")
			);
			DROP TABLE IF EXISTS "message";
			-- a message is created at a specific timestep
			--- to be send from a sender node to a receiver node
			CREATE TABLE IF NOT EXISTS "message" (
				"uuid"	UUID NOT NULL UNIQUE,
				"created_at"	INTEGER NOT NULL,
				"sender"	UUID NOT NULL,
				"receiver"	UUID NOT NULL,
				FOREIGN KEY("created_at") REFERENCES "timestep"("time") ON DELETE CASCADE ON UPDATE CASCADE,
				FOREIGN KEY("receiver") REFERENCES "node"("uuid"),
				FOREIGN KEY("sender") REFERENCES "node"("uuid"),
				PRIMARY KEY("uuid")
			);
			--- a signal is the processing of a message per timestep
			--- a signal references a message and is send via an edge
			--- a node takes an signal from an incoming edge and
			--- produces a new signal for the same message on an outgoing edge
			DROP TABLE IF EXISTS "signal";
			CREATE TABLE IF NOT EXISTS "signal" (
				"uuid"	UUID NOT NULL UNIQUE,
				"message"	UUID NOT NULL,
				"edge"	UUID NOT NULL,
				"transmitted_at"	INTEGER NOT NULL,
				UNIQUE("edge","transmitted_at"),
				FOREIGN KEY("message") REFERENCES "message"("uuid") ON UPDATE CASCADE ON DELETE CASCADE,
				FOREIGN KEY("edge") REFERENCES "edge"("uuid") ON UPDATE CASCADE ON DELETE CASCADE,
				FOREIGN KEY("transmitted_at") REFERENCES "timestep"("time") ON UPDATE CASCADE ON DELETE CASCADE,
				PRIMARY KEY("uuid")
			);

			--- an acknowledgement is a feedback reporting the success of a message
			--- the state of an acknowledgment can either be:
			--- 1 (success, the message reached the receiver)
			--- 2 (cyclic, the signal reached a node that already processed a signal of the same message but is waiting for an acknowlegment itself)
			--- 3 (deadend, the current target of the signal has no more outgoing edges left on which the signal could be send)
			DROP TABLE IF EXISTS "acknowledgment";
			CREATE TABLE IF NOT EXISTS "acknowledgment" (
				"uuid"	UUID NOT NULL UNIQUE,
				"signal"	UUID NOT NULL UNIQUE,
				"acked_at"	INTEGER NOT NULL,
				"state"	INTEGER NOT NULL,
				FOREIGN KEY("signal") REFERENCES "signal"("uuid") ON UPDATE CASCADE ON DELETE CASCADE,
				FOREIGN KEY("acked_at") REFERENCES "timestep"("time") ON UPDATE CASCADE ON DELETE CASCADE,
				PRIMARY KEY("uuid")
			);

			--- each node can have a 2D position for display purposes
			DROP TABLE IF EXISTS "node_position";
			CREATE TABLE IF NOT EXISTS "node_position" (
				"uuid"	UUID NOT NULL,
				"node"	UUID NOT NULL UNIQUE,
				"x"	INTEGER NOT NULL,
				"y"	INTEGER NOT NULL,
				FOREIGN KEY("node") REFERENCES "node"("uuid") ON DELETE CASCADE ON UPDATE CASCADE,
				PRIMARY KEY("uuid")
			);


			--- VIEWS

			--- pairs of symmetric edges
			DROP VIEW IF EXISTS "node_pair_time";
			CREATE VIEW node_pair_time AS SELECT MAX(time) AS time, sender.uuid AS sender, receiver.uuid AS receiver FROM timestep INNER JOIN node sender INNER JOIN node receiver WHERE sender.uuid <> receiver.uuid GROUP BY sender.uuid, receiver.uuid ORDER BY RANDOM();
			DROP VIEW IF EXISTS "reverse_edge";
			CREATE VIEW reverse_edge AS
			SELECT 
			edge.uuid AS uuid, 
			reverse.uuid AS reverse_uuid, 
			edge.source AS source, 
			edge.target AS target, 
			edge.delay AS delay, 
			reverse.delay AS reverse_delay
			FROM edge 
			LEFT JOIN edge reverse
			ON (edge.target, edge.source) = (reverse.source, reverse.target);
			

			--- the workload of each node per timestep
			--- calculated by summing the number of outgoing signals and
			--- the acknowledgments of incoming signals per timestep
			DROP VIEW IF EXISTS "node_workload";
			CREATE VIEW node_workload AS
			SELECT timestep.time, 
			node.uuid, 
			node.capacity AS max_load, 
			COUNT(DISTINCT signal.uuid) + COUNT(DISTINCT acknowledgment.uuid) as current_load, 
			COUNT(DISTINCT signal.uuid) AS current_load_signal, 
			COUNT(DISTINCT acknowledgment.uuid) AS current_load_ack
			FROM node 
			INNER JOIN timestep 
			LEFT JOIN edge out_edge ON out_edge.source = node.uuid
			LEFT JOIN edge in_edge ON in_edge.target = node.uuid
			LEFT JOIN signal ON (signal.edge, signal.transmitted_at) = (out_edge.uuid, timestep.time)
			LEFT JOIN signal ack_signal ON ack_signal.edge = in_edge.uuid
			LEFT JOIN acknowledgment ON (acknowledgment.signal, acknowledgment.acked_at) = (ack_signal.uuid,  timestep.time)
			GROUP BY node.uuid, timestep.time
			ORDER BY timestep.time, node.uuid;
			DROP VIEW IF EXISTS "global_message_health";


			--- list of all messages in the system
			--- counting all the corresponding signals and acnowledgments
			--- can be used to find wrong processed messages
			CREATE VIEW global_message_health AS
			SELECT message.*, 
			COUNT(DISTINCT ack_ok.uuid) > 0 AS progress,
			COUNT(DISTINCT initial_signal.uuid) AS trys,
			COUNT(DISTINCT ack_ok.uuid) AS ack_ok,
			COUNT(DISTINCT ack_trapped.uuid) AS ack_trapped,
			COUNT(DISTINCT ack_cyclic.uuid) AS ack_cyclic,
			COUNT(DISTINCT all_signal.uuid) AS signal_count,
			COUNT(DISTINCT all_ack.uuid) AS ack_count,
			MIN(all_signal.transmitted_at) AS first_trans,
			MAX(all_signal.transmitted_at) AS last_trans
			FROM message
			LEFT JOIN edge initial_edge ON initial_edge.source = message.sender
			LEFT JOIN signal initial_signal On (initial_signal.edge, initial_signal.message) = (initial_edge.uuid, message.uuid)
			LEFT JOIN signal all_signal ON all_signal.message = message.uuid
			LEFT JOIN acknowledgment all_ack ON all_ack.signal = all_signal.uuid
			LEFT JOIN acknowledgment ack_ok On (ack_ok.signal, ack_ok.state) = (initial_signal.uuid, 1)
			LEFT JOIN acknowledgment ack_trapped On (ack_trapped.signal, ack_trapped.state) = (initial_signal.uuid, 3)
			LEFT JOIN acknowledgment ack_cyclic On (ack_cyclic.signal, ack_cyclic.state) = (initial_signal.uuid, 2)
			GROUP BY message.uuid
			ORDER BY message.created_at ASC, message.uuid ASC;


			--- signals that have not been acknowledged yet
			--- but that either reached there goal, caused a cycle, are trapped
			--- or whose success signal has been acknowledged
			DROP VIEW IF EXISTS "available_response";
			CREATE VIEW available_response AS 
			SELECT *, 
			CASE WHEN deadend OR cycle_detected OR goal THEN self_ready_at WHEN ((excess_edges OR outstanding_ack) AND outgoing_state=3) THEN NULL ELSE outgoing_ready_at END AS ready_at,
			CASE WHEN goal THEN 1 WHEN cycle_detected THEN 2 WHEN deadend THEN 3 WHEN ((excess_edges OR outstanding_ack) AND outgoing_state=3) THEN NULL  ELSE outgoing_state END AS state
			FROM (
			SELECT 
			message.uuid AS message,
			current_node.uuid AS node,
			incoming_signal.uuid AS signal,
			node_workload.current_load AS current_load,
			node_workload.max_load AS max_load,
			current_node.uuid = message.receiver AS goal,
			MAX(outgoing_ack.acked_at) + outgoing_edge.delay AS outgoing_ready_at,
			incoming_signal.transmitted_at AS transmitted_at,
			incoming_signal.transmitted_at + incoming_edge.delay AS self_ready_at,
			SUM(COALESCE(incoming_signal.transmitted_at >= outgoing_signal.transmitted_at, 0)) AS cycle_detected,
			COUNT(DISTINCT outgoing_edge.uuid) < 1 AS deadend,
			MIN(outgoing_ack.state) AS outgoing_state,
			current_timestep.time AS time,
			COUNT(DISTINCT outgoing_signal.uuid) < COUNT(DISTINCT outgoing_edge.uuid) AS excess_edges,
			COUNT(DISTINCT outgoing_ack.uuid) < COUNT(DISTINCT outgoing_signal.uuid) AS outstanding_ack
			FROM message 
			INNER JOIN (SELECT MAX(time) AS time FROM timestep) current_timestep
			INNER JOIN signal incoming_signal ON incoming_signal.message = message.uuid 
			LEFT JOIN acknowledgment incoming_ack ON incoming_ack.signal = incoming_signal.uuid 
			INNER JOIN edge incoming_edge ON incoming_edge.uuid = incoming_signal.edge 
			INNER JOIN node current_node ON incoming_edge.target = current_node.uuid
			LEFT JOIN node_workload ON (node_workload.uuid, node_workload.time) = (current_node.uuid, current_timestep.time)
			LEFT JOIN edge outgoing_edge ON outgoing_edge.source = current_node.uuid AND current_node.uuid IS NOT message.receiver AND outgoing_edge.target IS NOT incoming_edge.source
			LEFT JOIN signal outgoing_signal ON (outgoing_signal.message, outgoing_signal.edge) = (incoming_signal.message, outgoing_edge.uuid) 
			LEFT JOIN acknowledgment outgoing_ack ON outgoing_ack.signal = outgoing_signal.uuid
			WHERE incoming_ack.uuid IS NULL
			GROUP BY message.uuid, incoming_signal.uuid
			) i;



			--- signals that have not reached thair goal but have not been
			--- further redirected to the next edge either 
			DROP VIEW IF EXISTS "available_redirect";
			CREATE VIEW available_redirect AS
			SELECT 
			incoming_signal.uuid AS signal,
			message.uuid AS message,
			current_node.uuid AS node,
			incoming_edge.uuid AS incoming_edge,
			outgoing_edge.uuid AS outgoing_edge,
			incoming_signal.transmitted_at AS transmitted_at,
			incoming_edge.delay AS delay,
			MAX(MAX(COALESCE(old_outgoing_ack.acked_at + old_outgoing_edge.delay, 0)), incoming_signal.transmitted_at + incoming_edge.delay) AS ready_at,
			MAX(MAX(COALESCE(old_outgoing_ack.acked_at + old_outgoing_edge.delay, 0)), incoming_signal.transmitted_at + incoming_edge.delay) <= current_timestep.time AS ready,
			node_workload.current_load AS current_load,
			node_workload.max_load AS max_load,
			COUNT(DISTINCT outgoing_edge.uuid) < 1 AS trapped,
			COUNT(DISTINCT outgoing_signal.uuid),
			current_timestep.time AS time
			FROM message 
			INNER JOIN (SELECT MAX(time) AS time FROM timestep) current_timestep
			INNER JOIN signal incoming_signal ON incoming_signal.message = message.uuid 
			LEFT JOIN acknowledgment incoming_ack ON incoming_ack.signal = incoming_signal.uuid
			INNER JOIN edge incoming_edge ON incoming_edge.uuid = incoming_signal.edge 
			INNER JOIN node current_node ON incoming_edge.target = current_node.uuid
			LEFT JOIN node_workload ON (node_workload.uuid, node_workload.time) = (current_node.uuid, current_timestep.time)
			LEFT JOIN edge outgoing_edge ON outgoing_edge.source = current_node.uuid 
			LEFT JOIN signal outgoing_signal ON (outgoing_signal.message, outgoing_signal.edge) = (incoming_signal.message, outgoing_edge.uuid) 
			LEFT JOIN acknowledgment outgoing_ack ON outgoing_ack.signal = outgoing_signal.uuid
			LEFT JOIN edge old_outgoing_edge ON old_outgoing_edge.source = current_node.uuid 
			LEFT JOIN signal old_outgoing_signal ON (old_outgoing_signal.message, old_outgoing_signal.edge) = (incoming_signal.message, old_outgoing_edge.uuid) 
			LEFT JOIN acknowledgment old_outgoing_ack ON old_outgoing_ack.signal = old_outgoing_signal.uuid
			WHERE message.receiver <> current_node.uuid AND outgoing_edge.target <> incoming_edge.source AND incoming_ack.uuid IS NULL AND (outgoing_signal.uuid IS NULL)
			GROUP BY message.uuid, current_node.uuid, outgoing_edge.uuid
			HAVING SUM(old_outgoing_signal.uuid IS NOT NULL AND old_outgoing_ack.state IS NOT 3 AND old_outgoing_ack.state IS NOT 2) < 1
			ORDER BY current_node.uuid, message.uuid, trapped ASC;

			--- messages that have not been transmitted at all
			--- or those of which all initial signals have been acknowledged
			--- negatively
			DROP VIEW IF EXISTS "available_dispatch";
			CREATE VIEW available_dispatch AS 
			SELECT message.created_at AS created_at,
			message.uuid AS uuid, 
			message.sender AS sender, 
			message.receiver AS receiver, 
			outgoing_edge.uuid AS edge,
			outgoing_edge.uuid IS NULL AS trapped,
			message.created_at AS created_at,
			sender.capacity AS max_load,
			node_workload.current_load AS current_load,
			current_timestep.time AS timeStep,
			old_outgoing_ack.uuid IS NOT NULL AS needs_retry,
			MAX(MAX(COALESCE(old_outgoing_ack.acked_at + old_outgoing_edge.delay, 0)), created_at) AS ready_at,
			MAX(MAX(COALESCE(old_outgoing_ack.acked_at + old_outgoing_edge.delay, 0)), created_at) <= current_timestep.time AS ready
			FROM message 
			INNER JOIN (SELECT MAX(time) AS time FROM timestep) current_timestep
			INNER JOIN node sender ON sender.uuid = message.sender 
			LEFT JOIN edge outgoing_edge ON outgoing_edge.source = message.sender
			LEFT JOIN signal outgoing_signal ON (outgoing_signal.edge, outgoing_signal.message) = (outgoing_edge.uuid, message.uuid)

			LEFT JOIN edge old_outgoing_edge ON old_outgoing_edge.source = message.sender
			LEFT JOIN signal old_outgoing_signal ON (old_outgoing_signal.edge, old_outgoing_signal.message) = (old_outgoing_edge.uuid, message.uuid)
			LEFT JOIN acknowledgment old_outgoing_ack ON old_outgoing_ack.signal = old_outgoing_signal.uuid

			LEFT JOIN node_workload ON (node_workload.uuid, node_workload.time) = (sender.uuid, current_timestep.time)
			WHERE outgoing_signal.uuid IS NULL
			GROUP BY message.uuid, outgoing_edge.uuid
			HAVING SUM(old_outgoing_signal.uuid IS NOT NULL AND old_outgoing_ack.state IS NOT 3 AND old_outgoing_ack.state IS NOT 2) = 0
			ORDER BY trapped ASC;
			DROP VIEW IF EXISTS "next_timestep";

			--- the next timestep at which further processing can take place
			CREATE VIEW next_timestep AS
			SELECT CASE WHEN current_timestep.time IS NULL THEN 0 WHEN MAX(at_limit) THEN current_timestep.time + 1 ELSE MIN(ready_at) END AS time FROM
			(
			SELECT ready_at, current_load >= max_load AS at_limit FROM available_dispatch
			UNION
			SELECT ready_at, current_load >= max_load AS at_limit FROM available_redirect
			UNION
			SELECT ready_at, current_load >= max_load AS at_limit FROM available_response
			) waiting
			INNER JOIN (SELECT MAX(time) AS time FROM timestep) current_timestep;

			--- try creating some indices for performance
			CREATE INDEX "signal_time_index" ON "signal" ("transmitted_at"	ASC);
			CREATE INDEX "message_time_index" ON "message" ("created_at"	ASC);
			CREATE INDEX "ack_time_index" ON "acknowledgment" ("acked_at"	ASC);
			CREATE INDEX "timestep_time" ON "timestep" ("time"	ASC);
			CREATE INDEX "edge_source" ON "edge" ("source"	ASC);
			CREATE INDEX "edge_target" ON "edge" ("target"	ASC);
			COMMIT;
		''')

		print("Schema created successfully");

		cursor = conn.cursor()

		# Create 5 Nodes/Agents
		nodes = [
			# id, capacity, position
			(uuid.uuid4(), 1, (0, 0)),
			(uuid.uuid4(), 1, (0, 200)),
			(uuid.uuid4(), 1, (200, 0)),
			(uuid.uuid4(), 1, (200, 200)),
			(uuid.uuid4(), 1, (100, 100)),
		]
		cursor.executemany('INSERT INTO node(uuid, capacity) VALUES (?, ?)', [
			(uid, cap) for (uid, cap, _) in nodes
		])

		cursor.executemany('INSERT INTO node_position(uuid, node, x, y) VALUES (?, ?, ?, ?)', [
			(uuid.uuid4(), node, x, y) for (node, cap, (x,y)) in nodes
		])

		# Create 5 Edges + 5 Reverse Edges
		connections = [
			# sources, target, delay
			(0, 1, 1),
			(0, 2, 1),
			(0, 3, 1),
			(2, 4, 1),
			(3, 4, 1),
		]
		edges = [
			(uid, source, target, delay)
			for (a,b,delay)
			in connections
			for (uid, source, target, delay) in
			[
				(uuid.uuid4(), nodes[a][0], nodes[b][0], delay),
				(uuid.uuid4(), nodes[b][0], nodes[a][0], delay)
			]
		]

		cursor.executemany('''
			INSERT INTO edge(uuid, source, target, delay) 
			VALUES (?, ?, ?, ?)
		''', edges)

		conn.commit()

		# create graph from nodes and edges in DB
		G = nx.DiGraph()
		cursor.execute('''SELECT uuid FROM node''')
		G.add_nodes_from([id for (id,) in cursor.fetchall()])
		cursor.execute('''SELECT source, target FROM edge''')
		G.add_edges_from(cursor.fetchall())

		# calculate node layout postions automatically
		pos=nx.spring_layout(G)
		# persist layout positions in database
		cursor.executemany('''
				UPDATE node_position SET x=?,y=? WHERE node = ?
			''', [(300*x, 300*y, node) for node, (x,y) in pos.items()])
		conn.commit()


		#####
		# SIMULATION
		#####


		# insert initial timestep
		cursor.execute('''
			INSERT INTO timestep(time) SELECT time 
			FROM next_timestep WHERE time is NOT NULL 
			ON CONFLICT DO NOTHING
		''')

		# run simulation for 500 steps
		for t in range(0, 100):
			action_counter = 0

			# show current timestep
			cursor.execute('''
				SELECT MAX(time) time FROM timestep
			''')
			(currentTime,) = cursor.fetchone()
			print(f"Time {currentTime}")

			# create new timestep
			cursor.execute('''
				INSERT INTO timestep(time) SELECT time FROM next_timestep WHERE time is NOT NULL ON CONFLICT DO NOTHING
			''')

			# try to send a random message that has not been sent yet
			cursor.execute('''
				INSERT INTO signal 
				SELECT ? AS signal_uuid, uuid as message, edge as edge, timeStep AS time 
				FROM available_dispatch WHERE current_load < max_load AND NOT trapped 
				ORDER BY created_at ASC, RANDOM() LIMIT 1
			''', (uuid.uuid4(),))

			# check if any message has been sent
			if cursor.rowcount > 0:
				print("one message dispatch")
				action_counter += 1


			# try to forward a random signal 
			cursor.execute('''
				INSERT INTO signal(uuid, message, edge, transmitted_at) 
				SELECT ?, message, outgoing_edge, time FROM available_redirect 
				WHERE ready and current_load < max_load AND NOT trapped ORDER BY RANDOM() LIMIT 1
			''', (uuid.uuid4(),))

			if cursor.rowcount > 0:
				print("one signal transmitted")
				action_counter += 1

			# try to ackowlege a signal
			cursor.execute('''
				INSERT INTO acknowledgment(uuid, signal, acked_at, state) 
				SELECT ?, signal, time, state FROM available_response
				WHERE current_load < max_load AND ready_at <= time
				ORDER BY RANDOM() LIMIT 1
			''', (uuid.uuid4(),))

			if cursor.rowcount > 0:
				print("one signal acknowledged")
				action_counter += 1


			# if no action took place this timestep, create a new random message
			# if anything took place create a new random message by chance
			if(action_counter < 1 or random() > 1):
				cursor.execute('''
					INSERT INTO message (uuid, created_at, sender, receiver) 
					SELECT  ?, time, sender, receiver FROM node_pair_time LIMIT 1
				''', (uuid.uuid4(),))
				print("created message")

			
			conn.commit()


		# export in-memory-db to file
		with sqlite3.connect('file:output.db', detect_types=sqlite3.PARSE_DECLTYPES,uri=True) as backup:
			conn.backup(backup)