import random
import random as rnd
from model.model import *
import uuid
import networkx as nx
import matplotlib.pyplot as plt
from tqdm import tqdm


class NetworkGenerator:

    def generate(self, min_edges, max_edges, min_nodes, max_nodes, max_delay, messages):
        nodes = []
        edges = []
        cluster = []

        label = 0

        # select random number of nodes and edges
        nbr_nodes = rnd.randrange(min_nodes, max_nodes)
        nbr_edges = rnd.randrange(min_edges, max_edges)

        # generate nodes
        for i in range(nbr_nodes):
            uid, delay = self.generate_properties(max_delay)
            node = Node(delay=delay, uid=uid, label=label, messages=messages)
            # append to node list
            nodes.append(node)
            label += 1

        cluster.append(set(nodes)) # set 0, all unconnected nodes
        cluster.append(set()) # set 1, put all nodes with minimum 1 connection

        # generate edges
        for i in range(nbr_edges):
            # assign to forward edge 
            uid, delay = self.generate_properties(max_delay)
            node_1, node_2 = self.choose_nodes(nodes, cluster[0], cluster[1])
            edge_1 = Edge(delay=delay, uid=uid, messages=messages, source_node=node_1, target_node=node_2)
            edge_1.incoming_edges.append(node_1)
            edge_1.outgoing_edges.append(node_2)
            edges.append(edge_1)

            # assign to backward edge
            uid = uuid.uuid4()
            edge_2 = Edge(delay=delay, uid=uid, messages=messages, source_node=node_2, target_node=node_1)
            edge_2.incoming_edges.append(node_2)
            edge_2.outgoing_edges.append(node_1)
            edges.append(edge_2)

            # assign edges to nodes
            index = nodes.index(node_1)
            nodes[index].incoming_edges.append(edge_2)
            nodes[index].outgoing_edges.append(edge_1)
            index = nodes.index(node_2)
            nodes[index].incoming_edges.append(edge_1)
            nodes[index].outgoing_edges.append(edge_2)

        helper_edges = 0

        # connect unconnected nodes
        while len(cluster[0]) > 0:
            uid, delay = self.generate_properties(max_delay)
            node_1, node_2 = self.choose_nodes(nodes, cluster[0], cluster[1], True)
            edge_1 = Edge(delay=delay, uid=uid, messages=messages, source_node=node_1, target_node=node_2)
            edge_1.incoming_edges.append(node_1)
            edge_1.outgoing_edges.append(node_2)
            edges.append(edge_1)

            uid = uuid.uuid4()
            edge_2 = Edge(delay=delay, uid=uid, messages=messages, source_node=node_2, target_node=node_1)
            edge_2.incoming_edges.append(node_2)
            edge_2.outgoing_edges.append(node_1)
            edges.append(edge_2)

            index = nodes.index(node_1)
            nodes[index].incoming_edges.append(edge_2)
            nodes[index].outgoing_edges.append(edge_1)
            index = nodes.index(node_2)
            nodes[index].incoming_edges.append(edge_1)
            nodes[index].outgoing_edges.append(edge_2)

            helper_edges += 1

        print("Created " + str(helper_edges) + " helper edges to connect disjunct subgraphs")

        return nodes, edges

    def generate_properties(self, max_delay):
        return uuid.uuid4(), rnd.randrange(1, max_delay)

    def choose_nodes(self, nodes, cluster_idle, cluster_conn, join=False):
        node_1 = None
        node_2 = None

        if join:
            node_1 = random.choice(list(cluster_idle))
            node_2 = random.choice(list(cluster_conn))
        else:
            index = rnd.randrange(0, len(nodes))
            node_1 = nodes[index]
            index = rnd.randrange(0, len(nodes))
            node_2 = nodes[index]
            while node_1 == node_2:
                index = rnd.randrange(0, len(nodes))
                node_2 = nodes[index]

        if node_1 in cluster_idle:
            cluster_idle.remove(node_1)
            cluster_conn.add(node_1)
        if node_2 in cluster_idle:
            cluster_idle.remove(node_2)
            cluster_conn.add(node_2)

        return node_1, node_2