import queue
import random


class Agent:

    def __init__(self, delay, uid, messages):
        self.uid = uid
        self.delay = delay
        self.message_history = messages # To gather statistics

        self.process_msg = 0
        self.message = None
        self.queue = queue.Queue(maxsize=100) # TODO parameterize size
        self.incoming_edges = []
        self.outgoing_edges = []

    def notify(self, steps=1):
        # Node is active (There is a message to process)
        if self.message is not None:
            self.process_msg -= steps

            # processing ended
            if self.process_msg == 0:
                process = True
                # if current Node is receiver do not forward and gather statistics
                if self.message.receiver == self:
                    # TODO Gather more statistics about message?
                    self.message_history.append(self.message)
                    process = False

                # Check if new message available, set inactive if not
                if not self.queue.empty():
                    self.message = self.queue.get()
                    self.process_msg = self.delay
                else:
                    self.message = None

                # Forward message to an outgoing edge. NOTE: self.process() method is implemented in child classes
                if process:
                    self.process()

    def add_to_queue(self, message):
        # check if node blocked
        if self.queue.full():
            return False

        # If node not inactive add to queue
        if self.message is not None:
            self.queue.put(message)
        else:
            # If node was inactive, directly set message
            self.message = message
            self.process_msg = self.delay

        return True

    # Implement in child classes Node and Edge
    def process(self):
        raise NotImplementedError

    # Make python iterable interface happy :)
    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, Agent):
            return self.uid == other.uid
        return False

    def __hash__(self):
        return hash(self.uid)


class Node(Agent):

    def __init__(self, delay, uid, messages, label):
        super().__init__(delay, uid, messages)
        self.label = label

    def process(self):
        # TODO Add learning module logic here, currently uses random edge
        edge = random.choice(self.outgoing_edges)
        edge.add_to_queue(self.message)


class Edge(Agent):

    def __init__(self, delay, uid, messages, source_node, target_node):
        super().__init__(delay, uid, messages)
        self.source_node = source_node
        self.target_node = target_node

    # There is only one edge here so we can simply forward
    # TODO Add/Clearify handling if add_to_queue() method returns false
    def process(self):
        self.outgoing_edges[0].add_to_queue(self.message)


class Message:

    def __init__(self, time, sender, receiver):
        self.sender     = sender
        self.receiver   = receiver
        self.time       = time