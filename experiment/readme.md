# run simulation

```shell
$ python simulate.py
```

Results are saved in output.db sqlite file

# Plot Graph

Reads the output.db and plots it

````shell
$ python plot.py
```