import random
import random as rnd
from model.model import *
import uuid
import networkx as nx
import matplotlib.pyplot as plt
from tqdm import tqdm
from multiprocessing import Pool, TimeoutError
import time
import os
from model.network_generator import NetworkGenerator

COLORS = ['k', 'g', 'b', 'c', 'm', 'y', 'r']

class Simulator:

    def __init__(self):
        self.nodes      = []
        self.edges      = []
        self.messages   = []

        self.total_steps    = 1000000

        # nbr edges
        self.min_edges      = 350
        self.max_edges      = 600
        self.max_delay      = 100

        # nbr nodes
        self.min_nodes      = 50
        self.max_nodes      = 150

        # nbr messages
        self.min_messages   = 50
        self.max_messages   = 150

        self.step = 0
        self.time = 0
        self.network_generator = NetworkGenerator()
        self.graph = nx.Graph()

    def simulate(self):
        """
            Generate a network with random number of nodes and edges.
            Process number of steps, generating messages
            in [min_messages, max_messages] each step.
        """
        # Create network
        self.nodes, self.edges = self.network_generator.generate(self.min_edges, self.max_edges, self.min_nodes, self.max_nodes, self.max_delay, self.messages)

        print("Len of Nodes: " + str(len(self.nodes)))
        print("Len of Edges: " + str(int(len(self.edges) / 2)))

        # Main Loop
        with tqdm(total=self.total_steps) as pbar:
            while self.step < self.total_steps:
                # generate msgs
                if rnd.random() < 0.05:                    
                    self.generate_messages(self.step)
                # notify all
                self.step_forward()

                pbar.update(1)
                self.step += 1

        self.plot()

    def step_forward(self):
        # Notify all agents about new step
        agents = self.nodes + self.edges
        for agent in agents:
            agent.notify()

    def generate_messages(self, t):
        """
            Generate a message with rnd sender and receiver.
            Put to sender's queue and append to local list.
        """
        for i in range(self.min_messages, self.max_messages):
            node_1 = random.choice(self.nodes)
            node_2 = random.choice(self.nodes)
            while node_1 == node_2:
                node_2 = random.choice(self.nodes)

            new_message = Message(time=t, sender=node_1, receiver=node_2)
            index = self.nodes.index(node_1)
            self.nodes[index].add_to_queue(new_message)

    def plot(self):
        """
            Add nodes and edges to graph.
            Assign weight and color to edge
            according to its weight.
        """
        print("Processed total number of " + str(len(self.messages)) + " messages")

        for node in self.nodes:
            self.graph.add_node(node.label, object=node) # node_color='k')

        for edge in self.edges:
            # default color: black
            color = 'k'
            if edge.delay >= len(COLORS):
                # if delay >= 7: red
                color = COLORS[-1]
            else: # color selected by weight
                color = COLORS[int(edge.delay)-1]
            self.graph.add_edge(edge.source_node.label, edge.target_node.label, weight=edge.delay, object=edge, color=color)

        nx.convert_node_labels_to_integers(self.graph)
        nx.draw(self.graph, with_labels=True, font_weight='bold')
        plt.show()


if __name__ == '__main__':
    simulator = Simulator()
    simulator.simulate()
