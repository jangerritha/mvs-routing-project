import sqlite3
import uuid
from random import random

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pylab

sqlite3.register_converter('UUID', lambda b: uuid.UUID(bytes=b))
sqlite3.register_adapter(uuid.UUID, lambda u: u.bytes)

if __name__ == "__main__":

	with sqlite3.connect('file:output.db', detect_types=sqlite3.PARSE_DECLTYPES,uri=True) as conn:
		cursor = conn.cursor()
		G = nx.DiGraph()
		cursor.execute('''SELECT uuid FROM node''')
		G.add_nodes_from([id for (id,) in cursor.fetchall()])
		cursor.execute('''SELECT source, target FROM edge''')
		G.add_edges_from(cursor.fetchall())

		cursor.execute('''SELECT node,x,y FROM node_position''')

		pos=dict([(uid, (x,y)) for (uid, x, y) in cursor.fetchall()])

		nx.draw(G,pos, node_size=500)
		pylab.show()